package sixthLessonHomeTask;

import java.util.Scanner;

public class Main {
    public static Dealership car [] = new Dealership[4];
    public static ClientData client[] = new ClientData[5];
    public static ClientData.Bonus bonusProgramm[] = new ClientData.Bonus[5];
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args){
        Main.initilization();

        System.out.println("\nWelcome to Volvo Dealership! Thank you for choosing us. Volvo ® - For life.");
        System.out.println("\nYou can choose interesting information for you." +
                "\n1 - New Arrivals" +
                "\n2 - Dealer's Clients" +
                "\n3 - Book car" +
                "\n4 - Bought cars");

        System.out.print("Please enter what you need: ");
        int In = scanner.nextInt();

        System.out.print("Your choice: ");
        if (In == 1){
            System.out.print("Look New Arrivals");
            showOne();
        }
        else if (In == 2){
            System.out.print("Look Dealership's Clients");
            showTwo();
        }
        else if (In == 3){
            System.out.print("Book car");
            showThree();
        }
        else if (In == 4){
            System.out.print("Look Bought cars");
            showFour();
        }
    }

    public static void initilization(){
        car[0] = new Dealership("Volvo XC90", 2.0, 7.8,
                "off-road", 2017);
        car[1] = new Dealership("Volvo V60", 2.0, 7.9,
                "station wagon", 2018);
        car[2] = new Dealership("Volvo S90", 2.0, 6.8,
                "sedan", 2016);
        car[3] = new Dealership("Volvo C70", 2.4, 9.0,
                "cabriolet", 2017);

        client[0] = new ClientData("Volvo XC90", "Will Smith");
        client[1] = new ClientData("Volvo C70", "Johnny Depp");
        client[2] = new ClientData("Volvo V60", "Tom Hanks");
        client[3] = new ClientData("Volvo S90", "Emma Watson");
        client[4] = new ClientData("Volvo C70", "Penelopa Cruz");

        bonusProgramm[0] = new ClientData.Bonus("25/02/2017");
        bonusProgramm[1] = new ClientData.Bonus("01/06/2017");
        bonusProgramm[2] = new ClientData.Bonus("16/03/2018");
        bonusProgramm[3] = new ClientData.Bonus("30/04/2016");
        bonusProgramm[4] = new ClientData.Bonus("09/10/2017");
    }

    public static void showOne(){
        int godnota [] = new int[5];
        System.out.println("\n\nCar\t\t\t\tVolume\tFast\tYear\t\tBoddy");
        for (int i = 0; i < car.length; i++){
            System.out.println("\n" + "\u001B[32m" + car[i].showInformation() + "\u001B[0m");
            for (int h = 0; h < godnota.length; h++){
                godnota[h] = car[i].showGodnota(h);
                for (int j = 0; j < godnota[h]; j++){
                    System.out.print("-");
                }
                System.out.print("\t\t");
            }
        }
    }

    public static void showTwo(){
        System.out.println("\n\nCar\t\t\t\tName");
        for (int i = 0; i < client.length; i++){
            int godnota[] = new int[2];
            System.out.println("\n" + "\u001B[35m" + client[i].showInformation() + "\u001B[0m");
            for (int h = 0; h < godnota.length; h++){
                godnota[h] = client[i].showGodnota(h);
                for (int j = 0; j < godnota[h]; j++){
                    System.out.print("-");
                }
                System.out.print("\t\t");
            }
        }
    }

    public static void showThree(){
        System.out.println("\n\nYou can book next car:");
        showOne();

        System.out.print("\nEnter your name: ");
        String firstNane = scanner.nextLine();
        String surName = scanner.nextLine();
        System.out.println("Your name: " + "\u001B[32m" + firstNane + surName + "\u001B[0m");
        System.out.print("Choose car which you like: ");
        String yourCar = scanner.next();
        System.out.print("\nYour car: ");
        if (yourCar.equals("C70")){
            yourCar = "Volvo C70";
        }
        if (yourCar.equals("XC90")){
            yourCar = "Volvo XC90";
        }
        if (yourCar.equals("V60")){
            yourCar = "Volvo V60";
        }
        if (yourCar.equals("S90")){
            yourCar = "Volvo S90";
        }
        System.out.println("\u001B[32m" + yourCar + "\u001B[0m");
//        ClientData creatNewClient[] = new ClientData[6];
//        for (int i = 0; i < creatNewClient.length; i++){
//            if (i == 5){
//                creatNewClient[5] =
//                        new ClientData(yourCar, firstNane + surName);
//                System.out.print("\n\u001B[34m");
//            }
//            else{
//                creatNewClient[i] = client[i];
//            }
//            System.out.println(creatNewClient[i].showInformation());
//        }
        ClientData youClient = new ClientData(yourCar, firstNane + surName);
        showTwo();
        System.out.println("\n\u001B[34m" + youClient.showInformation());


    }

    public static void showFour(){
        int godnota[] = new int[2];
        System.out.println("\n\nDate\t\t\tCar\t\t\t\tName");
        for (int i = 0; i < bonusProgramm.length; i++) {
            System.out.print("\n" + bonusProgramm[i].showInformation());
            System.out.print("\u001B[31m" + client[i].showInformation() + "\u001B[0m" + "\n");
            for (int h = 0; h < godnota.length; h++){
                if (h == 0){
                    godnota[0] = bonusProgramm[i].showGodnota(0);
                    for (int j = 0; j < godnota[0]; j++) {
                        System.out.print("-");
                    }
                }
                System.out.print("\t\t");
                godnota[h] = client[i].showGodnota(h);
                for (int j = 0; j < godnota[h]; j++){
                    System.out.print("-");
                }
            }
        }
    }
}
