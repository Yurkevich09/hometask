package sixthLessonHomeTask;

 class Dealership {
    protected String carBrand;
    protected double ingineVolume;
    protected double accelerationTo100;
    protected String bodyType;
    protected int releaseYear;


    public Dealership(String carBrand, double ingineVolume, double accelerationTo100,
                      String bodyType, int releaseYear){
        this.carBrand = carBrand;
        this.ingineVolume = ingineVolume;
        this.accelerationTo100 = accelerationTo100;
        this.bodyType = bodyType;
        this.releaseYear = releaseYear;
    }

    public Dealership(String carBrand) {
        this.carBrand = carBrand;
    }


    public String showInformation(){
        return carBrand + "\t\t" + ingineVolume + "\t\t" + accelerationTo100 + "\t\t"
                + releaseYear + "\t\t" + bodyType;
    }

    public int showGodnota(int k){
        if (k == 0){
            return carBrand.length();
        }
        else if (k == 1){
            return String.valueOf(ingineVolume).length();
        }
        else if (k == 2){
            return String.valueOf(accelerationTo100).length();
        }
        else if (k == 3){
            return String.valueOf(releaseYear).length();
        }
        else {
            return bodyType.length();
        }
    }

}
