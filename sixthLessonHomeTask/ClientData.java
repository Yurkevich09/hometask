package sixthLessonHomeTask;

public class ClientData extends Dealership{
    protected String nameClient;

    public ClientData(String carBrand,  String nameClient) {
        super(carBrand);
        this.nameClient = nameClient;
    }

    public String showInformation(){
        return carBrand + "\t\t" + nameClient;
    }

    static class Bonus{
        protected String datePurchase;

        public Bonus(String datePurchase) {
            this.datePurchase = datePurchase;
        }

        public String showInformation(){
            return datePurchase + "\t\t";
        }

        public int showGodnota(int k){
            if (k == 0){
                return datePurchase.length();
            }
            else {
                return 10;
            }
        }
    }

    public int showGodnota(int k){
        if (k == 0){
            return carBrand.length();
        }
        else {
            return nameClient.length();
        }
    }


}
